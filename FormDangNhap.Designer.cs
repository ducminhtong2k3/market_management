﻿namespace market_management
{
    partial class FrmDangNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SbtnDangNhap = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TeTenDangNhap = new DevExpress.XtraEditors.TextEdit();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.hide = new System.Windows.Forms.PictureBox();
            this.eye = new System.Windows.Forms.PictureBox();
            this.TbMatKhau = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.TeTenDangNhap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eye)).BeginInit();
            this.SuspendLayout();
            // 
            // SbtnDangNhap
            // 
            this.SbtnDangNhap.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.SbtnDangNhap.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.SbtnDangNhap.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SbtnDangNhap.Appearance.ForeColor = System.Drawing.Color.DarkBlue;
            this.SbtnDangNhap.Appearance.Options.UseBackColor = true;
            this.SbtnDangNhap.Appearance.Options.UseBorderColor = true;
            this.SbtnDangNhap.Appearance.Options.UseFont = true;
            this.SbtnDangNhap.Appearance.Options.UseForeColor = true;
            this.SbtnDangNhap.Location = new System.Drawing.Point(234, 348);
            this.SbtnDangNhap.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.SbtnDangNhap.Name = "SbtnDangNhap";
            this.SbtnDangNhap.Size = new System.Drawing.Size(110, 42);
            this.SbtnDangNhap.TabIndex = 20;
            this.SbtnDangNhap.Text = "Đăng nhập";
            this.SbtnDangNhap.Click += new System.EventHandler(this.SbtnDangNhap_Click);
            this.SbtnDangNhap.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SbtnDangNhap_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(219, 63);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(144, 38);
            this.labelControl3.TabIndex = 19;
            this.labelControl3.Text = "Đăng nhập";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(155, 233);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(76, 23);
            this.labelControl2.TabIndex = 18;
            this.labelControl2.Text = "Mật khẩu";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(155, 153);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(118, 23);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Tên đăng nhập";
            // 
            // TeTenDangNhap
            // 
            this.TeTenDangNhap.Location = new System.Drawing.Point(155, 181);
            this.TeTenDangNhap.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TeTenDangNhap.Name = "TeTenDangNhap";
            this.TeTenDangNhap.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeTenDangNhap.Properties.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.TeTenDangNhap.Properties.Appearance.Options.UseFont = true;
            this.TeTenDangNhap.Properties.Appearance.Options.UseForeColor = true;
            this.TeTenDangNhap.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.TeTenDangNhap.Size = new System.Drawing.Size(281, 28);
            this.TeTenDangNhap.TabIndex = 15;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::market_management.Properties.Resources.acc1;
            this.pictureBox1.Location = new System.Drawing.Point(115, 174);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(33, 34);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::market_management.Properties.Resources.free_key_icon_920_thumb;
            this.pictureBox3.Location = new System.Drawing.Point(115, 254);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(33, 37);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 23;
            this.pictureBox3.TabStop = false;
            // 
            // hide
            // 
            this.hide.BackColor = System.Drawing.Color.Transparent;
            this.hide.Image = global::market_management.Properties.Resources.eye22;
            this.hide.Location = new System.Drawing.Point(444, 255);
            this.hide.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.hide.Name = "hide";
            this.hide.Size = new System.Drawing.Size(33, 35);
            this.hide.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.hide.TabIndex = 24;
            this.hide.TabStop = false;
            this.hide.Click += new System.EventHandler(this.hide_Click);
            // 
            // eye
            // 
            this.eye.BackColor = System.Drawing.Color.Transparent;
            this.eye.Image = global::market_management.Properties.Resources.eye2;
            this.eye.Location = new System.Drawing.Point(444, 256);
            this.eye.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.eye.Name = "eye";
            this.eye.Size = new System.Drawing.Size(33, 35);
            this.eye.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.eye.TabIndex = 25;
            this.eye.TabStop = false;
            this.eye.Click += new System.EventHandler(this.eye_Click);
            // 
            // TbMatKhau
            // 
            this.TbMatKhau.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbMatKhau.Location = new System.Drawing.Point(155, 256);
            this.TbMatKhau.Name = "TbMatKhau";
            this.TbMatKhau.PasswordChar = '*';
            this.TbMatKhau.Size = new System.Drawing.Size(281, 28);
            this.TbMatKhau.TabIndex = 26;
            // 
            // FrmDangNhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
            this.BackgroundImageStore = global::market_management.Properties.Resources.nenDangNhap;
            this.ClientSize = new System.Drawing.Size(584, 475);
            this.Controls.Add(this.TbMatKhau);
            this.Controls.Add(this.eye);
            this.Controls.Add(this.hide);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.SbtnDangNhap);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.TeTenDangNhap);
            this.IconOptions.Image = global::market_management.Properties.Resources.bieuTuong;
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.MaximizeBox = false;
            this.Name = "FrmDangNhap";
            this.Text = "Form Đăng nhập";
            ((System.ComponentModel.ISupportInitialize)(this.TeTenDangNhap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eye)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton SbtnDangNhap;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit TeTenDangNhap;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox hide;
        private System.Windows.Forms.PictureBox eye;
        private System.Windows.Forms.TextBox TbMatKhau;
    }
}
﻿namespace market_management
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.fluentDesignFormContainer1 = new DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer();
            this.PnlMain = new DevExpress.XtraEditors.PanelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.accordionControl1 = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.accordionControlElement1 = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.SP = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.NV = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.KH = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.NCC = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.LoaiSP = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DangKy = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlSeparator1 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.accordionControlSeparator2 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.QLBanHang = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlSeparator3 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.QLNhapHang = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlSeparator4 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.QLMaGiamGia = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlSeparator5 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.BaoCaoThongKe = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.TKDoanhThu = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.TKHangTon = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.TKBanHang = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.accordionControlSeparator6 = new DevExpress.XtraBars.Navigation.AccordionControlSeparator();
            this.DangXuat = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.DangKy = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.fluentDesignFormControl1 = new DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl();
            this.BsiChucvu = new DevExpress.XtraBars.BarStaticItem();
            this.BsiTenNV = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.fluentFormDefaultManager1 = new DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager(this.components);
            this.fluentDesignFormContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PnlMain)).BeginInit();
            this.PnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentFormDefaultManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // fluentDesignFormContainer1
            // 
            this.fluentDesignFormContainer1.Controls.Add(this.PnlMain);
            this.fluentDesignFormContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fluentDesignFormContainer1.Location = new System.Drawing.Point(223, 31);
            this.fluentDesignFormContainer1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fluentDesignFormContainer1.Name = "fluentDesignFormContainer1";
            this.fluentDesignFormContainer1.Size = new System.Drawing.Size(804, 618);
            this.fluentDesignFormContainer1.TabIndex = 0;
            // 
            // PnlMain
            // 
            this.PnlMain.AutoSize = true;
            this.PnlMain.Controls.Add(this.labelControl8);
            this.PnlMain.Controls.Add(this.pictureBox3);
            this.PnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlMain.Location = new System.Drawing.Point(0, 0);
            this.PnlMain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(804, 618);
            this.PnlMain.TabIndex = 6;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseForeColor = true;
            this.labelControl8.Location = new System.Drawing.Point(232, 92);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(288, 32);
            this.labelControl8.TabIndex = 6;
            this.labelControl8.Text = "Hệ thống quản lý siêu thị";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(-21, 4);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(825, 582);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // accordionControl1
            // 
            this.accordionControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.accordionControl1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.accordionControlElement1,
            this.accordionControlSeparator1,
            this.accordionControlSeparator2,
            this.QLBanHang,
            this.accordionControlSeparator3,
            this.QLNhapHang,
            this.accordionControlSeparator4,
            this.QLMaGiamGia,
            this.accordionControlSeparator5,
            this.BaoCaoThongKe,
            this.accordionControlSeparator6,
            this.DangXuat,
            this.DangKy});
            this.accordionControl1.Location = new System.Drawing.Point(0, 31);
            this.accordionControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accordionControl1.Name = "accordionControl1";
            this.accordionControl1.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Touch;
            this.accordionControl1.Size = new System.Drawing.Size(223, 618);
            this.accordionControl1.TabIndex = 1;
            this.accordionControl1.ViewType = DevExpress.XtraBars.Navigation.AccordionControlViewType.HamburgerMenu;
            // 
            // accordionControlElement1
            // 
            this.accordionControlElement1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.SP,
            this.NV,
            this.KH,
            this.NCC,
            this.LoaiSP,
            this.DangKy});
            this.accordionControlElement1.Expanded = true;
            this.accordionControlElement1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("accordionControlElement1.ImageOptions.Image")));
            this.accordionControlElement1.Name = "accordionControlElement1";
            this.accordionControlElement1.Text = "Đăng ký tài khoản cho nhân viên";
            // 
            // SP
            // 
            this.SP.Name = "SP";
            this.SP.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.SP.Text = "Sản phẩm";
            this.SP.Click += new System.EventHandler(this.SP_Click);
            // 
            // NV
            // 
            this.NV.Name = "NV";
            this.NV.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.NV.Text = "Nhân viên";
            this.NV.Click += new System.EventHandler(this.NV_Click);
            // 
            // KH
            // 
            this.KH.Name = "KH";
            this.KH.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.KH.Text = "Khách hàng";
            this.KH.Click += new System.EventHandler(this.KH_Click);
            // 
            // NCC
            // 
            this.NCC.Name = "NCC";
            this.NCC.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.NCC.Text = "Nhà cung cấp";
            // 
            // LoaiSP
            // 
            this.LoaiSP.Name = "LoaiSP";
            this.LoaiSP.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.LoaiSP.Text = "Loại sản phẩm";
            this.LoaiSP.Click += new System.EventHandler(this.LoaiSP_Click);
            // 
            // DangKy
            // 
            this.DangKy.Name = "DangKy";
            this.DangKy.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DangKy.Text = "Đăng ký tài khoản cho nhân viên";
            this.DangKy.Click += new System.EventHandler(this.DangKy_Click_1);
            // 
            // accordionControlSeparator1
            // 
            this.accordionControlSeparator1.Name = "accordionControlSeparator1";
            // 
            // accordionControlSeparator2
            // 
            this.accordionControlSeparator2.Name = "accordionControlSeparator2";
            // 
            // QLBanHang
            // 
            this.QLBanHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("QLBanHang.ImageOptions.Image")));
            this.QLBanHang.Name = "QLBanHang";
            this.QLBanHang.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.QLBanHang.Text = "Quản lý bán hàng";
            // 
            // accordionControlSeparator3
            // 
            this.accordionControlSeparator3.Name = "accordionControlSeparator3";
            // 
            // QLNhapHang
            // 
            this.QLNhapHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("QLNhapHang.ImageOptions.Image")));
            this.QLNhapHang.Name = "QLNhapHang";
            this.QLNhapHang.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.QLNhapHang.Text = "Quản lý nhập hàng";
            this.QLNhapHang.Click += new System.EventHandler(this.QLNhapHang_Click);
            // 
            // accordionControlSeparator4
            // 
            this.accordionControlSeparator4.Name = "accordionControlSeparator4";
            // 
            // QLMaGiamGia
            // 
            this.QLMaGiamGia.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("QLMaGiamGia.ImageOptions.Image")));
            this.QLMaGiamGia.Name = "QLMaGiamGia";
            this.QLMaGiamGia.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.QLMaGiamGia.Text = "Quản lý mã giảm giá";
            this.QLMaGiamGia.Click += new System.EventHandler(this.QLMaGiamGia_Click);
            // 
            // accordionControlSeparator5
            // 
            this.accordionControlSeparator5.Name = "accordionControlSeparator5";
            // 
            // BaoCaoThongKe
            // 
            this.BaoCaoThongKe.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.TKDoanhThu,
            this.TKHangTon,
            this.TKBanHang});
            this.BaoCaoThongKe.Expanded = true;
            this.BaoCaoThongKe.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BaoCaoThongKe.ImageOptions.Image")));
            this.BaoCaoThongKe.Name = "BaoCaoThongKe";
            this.BaoCaoThongKe.Text = "Báo cáo thống kê";
            // 
            // TKDoanhThu
            // 
            this.TKDoanhThu.Name = "TKDoanhThu";
            this.TKDoanhThu.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.TKDoanhThu.Text = "Thống kê doanh thu";
            this.TKDoanhThu.Click += new System.EventHandler(this.TKDoanhThu_Click);
            // 
            // TKHangTon
            // 
            this.TKHangTon.Name = "TKHangTon";
            this.TKHangTon.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.TKHangTon.Text = "Thống kê hàng tồn kho";
            this.TKHangTon.Click += new System.EventHandler(this.TKHangTon_Click);
            // 
            // TKBanHang
            // 
            this.TKBanHang.Name = "TKBanHang";
            this.TKBanHang.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.TKBanHang.Text = "Thống kê bán hàng";
            this.TKBanHang.Click += new System.EventHandler(this.TKBanHang_Click);
            // 
            // accordionControlSeparator6
            // 
            this.accordionControlSeparator6.Name = "accordionControlSeparator6";
            // 
            // DangXuat
            // 
            this.DangXuat.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("DangXuat.ImageOptions.Image")));
            this.DangXuat.Name = "DangXuat";
            this.DangXuat.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DangXuat.Text = "Đăng xuất";
            this.DangXuat.Click += new System.EventHandler(this.DangXuat_Click);
            // 
            // DangKy
            // 
            this.DangKy.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("DangKy.ImageOptions.SvgImage")));
            this.DangKy.Name = "DangKy";
            this.DangKy.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.DangKy.Text = "Tạo tài khoản nhân viên";
            this.DangKy.Visible = false;
            this.DangKy.Click += new System.EventHandler(this.DangKy_Click);
            // 
            // fluentDesignFormControl1
            // 
            this.fluentDesignFormControl1.FluentDesignForm = this;
            this.fluentDesignFormControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.BsiChucvu,
            this.BsiTenNV,
            this.barStaticItem4});
            this.fluentDesignFormControl1.Location = new System.Drawing.Point(0, 0);
            this.fluentDesignFormControl1.Manager = this.fluentFormDefaultManager1;
            this.fluentDesignFormControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fluentDesignFormControl1.Name = "fluentDesignFormControl1";
            this.fluentDesignFormControl1.Size = new System.Drawing.Size(1027, 31);
            this.fluentDesignFormControl1.TabIndex = 2;
            this.fluentDesignFormControl1.TabStop = false;
            this.fluentDesignFormControl1.TitleItemLinks.Add(this.BsiChucvu);
            this.fluentDesignFormControl1.TitleItemLinks.Add(this.BsiTenNV);
            this.fluentDesignFormControl1.TitleItemLinks.Add(this.barStaticItem4);
            // 
            // BsiChucvu
            // 
            this.BsiChucvu.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.BsiChucvu.Caption = "Chức vụ";
            this.BsiChucvu.Id = 0;
            this.BsiChucvu.Name = "BsiChucvu";
            // 
            // BsiTenNV
            // 
            this.BsiTenNV.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.BsiTenNV.Caption = "Tên nhân viên";
            this.BsiTenNV.Id = 1;
            this.BsiTenNV.Name = "BsiTenNV";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem4.Id = 3;
            this.barStaticItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barStaticItem4.ImageOptions.Image")));
            this.barStaticItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barStaticItem4.ImageOptions.LargeImage")));
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // fluentFormDefaultManager1
            // 
            this.fluentFormDefaultManager1.Form = this;
            this.fluentFormDefaultManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.BsiChucvu,
            this.BsiTenNV,
            this.barStaticItem4});
            this.fluentFormDefaultManager1.MaxItemId = 4;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1027, 649);
            this.ControlContainer = this.fluentDesignFormContainer1;
            this.Controls.Add(this.fluentDesignFormContainer1);
            this.Controls.Add(this.accordionControl1);
            this.Controls.Add(this.fluentDesignFormControl1);
            this.FluentDesignFormControl = this.fluentDesignFormControl1;
            this.IconOptions.Image = global::market_management.Properties.Resources.bieuTuong;
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.NavigationControl = this.accordionControl1;
            this.Text = "Hệ thống quản lý siêu thị";
            this.fluentDesignFormContainer1.ResumeLayout(false);
            this.fluentDesignFormContainer1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PnlMain)).EndInit();
            this.PnlMain.ResumeLayout(false);
            this.PnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentFormDefaultManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormContainer fluentDesignFormContainer1;
        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl1;
        private DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl fluentDesignFormControl1;
        private DevExpress.XtraBars.Navigation.AccordionControlElement accordionControlElement1;
        private DevExpress.XtraBars.FluentDesignSystem.FluentFormDefaultManager fluentFormDefaultManager1;
        private DevExpress.XtraEditors.PanelControl PnlMain;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement SP;
        private DevExpress.XtraBars.Navigation.AccordionControlElement NV;
        private DevExpress.XtraBars.Navigation.AccordionControlElement KH;
        private DevExpress.XtraBars.Navigation.AccordionControlElement NCC;
        private DevExpress.XtraBars.Navigation.AccordionControlElement LoaiSP;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator1;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator2;
        private DevExpress.XtraBars.Navigation.AccordionControlElement QLBanHang;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement QLNhapHang;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator4;
        private DevExpress.XtraBars.Navigation.AccordionControlElement QLMaGiamGia;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator5;
        private DevExpress.XtraBars.Navigation.AccordionControlElement BaoCaoThongKe;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator6;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DangXuat;
        private DevExpress.XtraBars.Navigation.AccordionControlElement TKDoanhThu;
        private DevExpress.XtraBars.Navigation.AccordionControlElement TKHangTon;
        private DevExpress.XtraBars.Navigation.AccordionControlElement TKBanHang;
        private DevExpress.XtraBars.BarStaticItem BsiChucvu;
        private DevExpress.XtraBars.BarStaticItem BsiTenNV;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.Navigation.AccordionControlElement DangKy;
    }
}
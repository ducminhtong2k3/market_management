﻿namespace market_management
{
    partial class FrmThemKH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbDKTaiKhoan = new DevExpress.XtraEditors.LabelControl();
            this.GrbDangKy = new DevExpress.XtraEditors.GroupControl();
            this.BtnXoaThongTin = new DevExpress.XtraEditors.SimpleButton();
            this.BtnThem = new DevExpress.XtraEditors.SimpleButton();
            this.DeNgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.LbNgaySinh = new DevExpress.XtraEditors.LabelControl();
            this.CbeMaGiamGia = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CbeGioiTinh = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TeDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.LbDiaChi = new DevExpress.XtraEditors.LabelControl();
            this.TeSDT = new DevExpress.XtraEditors.TextEdit();
            this.LbSDT = new DevExpress.XtraEditors.LabelControl();
            this.LbGioiTinh = new DevExpress.XtraEditors.LabelControl();
            this.LbMaGiamGia = new DevExpress.XtraEditors.LabelControl();
            this.TeTenKH = new DevExpress.XtraEditors.TextEdit();
            this.LbTenKH = new DevExpress.XtraEditors.LabelControl();
            this.TeMaKH = new DevExpress.XtraEditors.TextEdit();
            this.LbMaKH = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.GrbDangKy)).BeginInit();
            this.GrbDangKy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgaySinh.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbeMaGiamGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbeGioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeSDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeTenKH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeMaKH.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LbDKTaiKhoan
            // 
            this.LbDKTaiKhoan.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LbDKTaiKhoan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.LbDKTaiKhoan.Appearance.Font = new System.Drawing.Font("Segoe UI", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbDKTaiKhoan.Appearance.ForeColor = System.Drawing.Color.Black;
            this.LbDKTaiKhoan.Appearance.Options.UseBackColor = true;
            this.LbDKTaiKhoan.Appearance.Options.UseFont = true;
            this.LbDKTaiKhoan.Appearance.Options.UseForeColor = true;
            this.LbDKTaiKhoan.Location = new System.Drawing.Point(382, 27);
            this.LbDKTaiKhoan.Name = "LbDKTaiKhoan";
            this.LbDKTaiKhoan.Size = new System.Drawing.Size(181, 31);
            this.LbDKTaiKhoan.TabIndex = 5;
            this.LbDKTaiKhoan.Text = "Thêm khách hàng";
            // 
            // GrbDangKy
            // 
            this.GrbDangKy.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GrbDangKy.Appearance.BackColor = System.Drawing.Color.Wheat;
            this.GrbDangKy.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.GrbDangKy.Appearance.Options.UseBackColor = true;
            this.GrbDangKy.Appearance.Options.UseFont = true;
            this.GrbDangKy.Controls.Add(this.BtnXoaThongTin);
            this.GrbDangKy.Controls.Add(this.BtnThem);
            this.GrbDangKy.Location = new System.Drawing.Point(91, 91);
            this.GrbDangKy.Name = "GrbDangKy";
            this.GrbDangKy.Size = new System.Drawing.Size(823, 313);
            this.GrbDangKy.TabIndex = 4;
            this.GrbDangKy.Text = "Thông tin khách hàng";
            // 
            // BtnXoaThongTin
            // 
            this.BtnXoaThongTin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnXoaThongTin.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnXoaThongTin.Appearance.Options.UseFont = true;
            this.BtnXoaThongTin.Location = new System.Drawing.Point(93, 261);
            this.BtnXoaThongTin.Name = "BtnXoaThongTin";
            this.BtnXoaThongTin.Size = new System.Drawing.Size(137, 23);
            this.BtnXoaThongTin.TabIndex = 14;
            this.BtnXoaThongTin.Text = "Xoá hết thông tin";
            this.BtnXoaThongTin.Click += new System.EventHandler(this.BtnXoaThongTin_Click);
            // 
            // BtnThem
            // 
            this.BtnThem.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnThem.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnThem.Appearance.Options.UseFont = true;
            this.BtnThem.Location = new System.Drawing.Point(368, 261);
            this.BtnThem.Name = "BtnThem";
            this.BtnThem.Size = new System.Drawing.Size(137, 23);
            this.BtnThem.TabIndex = 13;
            this.BtnThem.Text = "Thêm";
            this.BtnThem.Click += new System.EventHandler(this.BtnThem_Click);
            // 
            // DeNgaySinh
            // 
            this.DeNgaySinh.EditValue = null;
            this.DeNgaySinh.Location = new System.Drawing.Point(221, 256);
            this.DeNgaySinh.Name = "DeNgaySinh";
            this.DeNgaySinh.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeNgaySinh.Properties.Appearance.Options.UseFont = true;
            this.DeNgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeNgaySinh.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeNgaySinh.Size = new System.Drawing.Size(128, 22);
            this.DeNgaySinh.TabIndex = 77;
            // 
            // LbNgaySinh
            // 
            this.LbNgaySinh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbNgaySinh.Appearance.Options.UseFont = true;
            this.LbNgaySinh.Location = new System.Drawing.Point(125, 257);
            this.LbNgaySinh.Name = "LbNgaySinh";
            this.LbNgaySinh.Size = new System.Drawing.Size(54, 15);
            this.LbNgaySinh.TabIndex = 76;
            this.LbNgaySinh.Text = "Ngày Sinh";
            // 
            // CbeMaGiamGia
            // 
            this.CbeMaGiamGia.Location = new System.Drawing.Point(701, 208);
            this.CbeMaGiamGia.Name = "CbeMaGiamGia";
            this.CbeMaGiamGia.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.CbeMaGiamGia.Properties.Appearance.Options.UseFont = true;
            this.CbeMaGiamGia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CbeMaGiamGia.Size = new System.Drawing.Size(144, 22);
            this.CbeMaGiamGia.TabIndex = 75;
            // 
            // CbeGioiTinh
            // 
            this.CbeGioiTinh.Location = new System.Drawing.Point(459, 153);
            this.CbeGioiTinh.Name = "CbeGioiTinh";
            this.CbeGioiTinh.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.CbeGioiTinh.Properties.Appearance.Options.UseFont = true;
            this.CbeGioiTinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CbeGioiTinh.Size = new System.Drawing.Size(122, 22);
            this.CbeGioiTinh.TabIndex = 74;
            // 
            // TeDiaChi
            // 
            this.TeDiaChi.Location = new System.Drawing.Point(701, 153);
            this.TeDiaChi.Name = "TeDiaChi";
            this.TeDiaChi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.TeDiaChi.Properties.Appearance.Options.UseFont = true;
            this.TeDiaChi.Size = new System.Drawing.Size(144, 22);
            this.TeDiaChi.TabIndex = 73;
            // 
            // LbDiaChi
            // 
            this.LbDiaChi.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.LbDiaChi.Appearance.Options.UseFont = true;
            this.LbDiaChi.Location = new System.Drawing.Point(607, 154);
            this.LbDiaChi.Name = "LbDiaChi";
            this.LbDiaChi.Size = new System.Drawing.Size(36, 15);
            this.LbDiaChi.TabIndex = 72;
            this.LbDiaChi.Text = "Địa chỉ";
            // 
            // TeSDT
            // 
            this.TeSDT.Location = new System.Drawing.Point(459, 207);
            this.TeSDT.Name = "TeSDT";
            this.TeSDT.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.TeSDT.Properties.Appearance.Options.UseFont = true;
            this.TeSDT.Size = new System.Drawing.Size(122, 22);
            this.TeSDT.TabIndex = 71;
            // 
            // LbSDT
            // 
            this.LbSDT.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.LbSDT.Appearance.Options.UseFont = true;
            this.LbSDT.Location = new System.Drawing.Point(374, 210);
            this.LbSDT.Name = "LbSDT";
            this.LbSDT.Size = new System.Drawing.Size(69, 15);
            this.LbSDT.TabIndex = 70;
            this.LbSDT.Text = "Số điện thoại";
            // 
            // LbGioiTinh
            // 
            this.LbGioiTinh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.LbGioiTinh.Appearance.Options.UseFont = true;
            this.LbGioiTinh.Location = new System.Drawing.Point(374, 154);
            this.LbGioiTinh.Name = "LbGioiTinh";
            this.LbGioiTinh.Size = new System.Drawing.Size(45, 15);
            this.LbGioiTinh.TabIndex = 69;
            this.LbGioiTinh.Text = "Giới tính";
            // 
            // LbMaGiamGia
            // 
            this.LbMaGiamGia.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.LbMaGiamGia.Appearance.Options.UseFont = true;
            this.LbMaGiamGia.Location = new System.Drawing.Point(607, 210);
            this.LbMaGiamGia.Name = "LbMaGiamGia";
            this.LbMaGiamGia.Size = new System.Drawing.Size(66, 15);
            this.LbMaGiamGia.TabIndex = 68;
            this.LbMaGiamGia.Text = "Mã giảm giá";
            // 
            // TeTenKH
            // 
            this.TeTenKH.Location = new System.Drawing.Point(221, 209);
            this.TeTenKH.Name = "TeTenKH";
            this.TeTenKH.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.TeTenKH.Properties.Appearance.Options.UseFont = true;
            this.TeTenKH.Size = new System.Drawing.Size(128, 22);
            this.TeTenKH.TabIndex = 67;
            // 
            // LbTenKH
            // 
            this.LbTenKH.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.LbTenKH.Appearance.Options.UseFont = true;
            this.LbTenKH.Location = new System.Drawing.Point(125, 210);
            this.LbTenKH.Name = "LbTenKH";
            this.LbTenKH.Size = new System.Drawing.Size(87, 15);
            this.LbTenKH.TabIndex = 66;
            this.LbTenKH.Text = "Tên Khách Hàng";
            // 
            // TeMaKH
            // 
            this.TeMaKH.Location = new System.Drawing.Point(221, 153);
            this.TeMaKH.Name = "TeMaKH";
            this.TeMaKH.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.TeMaKH.Properties.Appearance.Options.UseFont = true;
            this.TeMaKH.Size = new System.Drawing.Size(128, 22);
            this.TeMaKH.TabIndex = 65;
            // 
            // LbMaKH
            // 
            this.LbMaKH.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.LbMaKH.Appearance.Options.UseFont = true;
            this.LbMaKH.Location = new System.Drawing.Point(125, 154);
            this.LbMaKH.Name = "LbMaKH";
            this.LbMaKH.Size = new System.Drawing.Size(85, 15);
            this.LbMaKH.TabIndex = 64;
            this.LbMaKH.Text = "Mã Khách Hàng";
            // 
            // FrmThemKH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 431);
            this.Controls.Add(this.DeNgaySinh);
            this.Controls.Add(this.LbNgaySinh);
            this.Controls.Add(this.CbeMaGiamGia);
            this.Controls.Add(this.CbeGioiTinh);
            this.Controls.Add(this.TeDiaChi);
            this.Controls.Add(this.LbDiaChi);
            this.Controls.Add(this.TeSDT);
            this.Controls.Add(this.LbSDT);
            this.Controls.Add(this.LbGioiTinh);
            this.Controls.Add(this.LbMaGiamGia);
            this.Controls.Add(this.TeTenKH);
            this.Controls.Add(this.LbTenKH);
            this.Controls.Add(this.TeMaKH);
            this.Controls.Add(this.LbMaKH);
            this.Controls.Add(this.LbDKTaiKhoan);
            this.Controls.Add(this.GrbDangKy);
            this.IconOptions.Image = global::market_management.Properties.Resources.bieuTuong;
            this.Name = "FrmThemKH";
            this.Text = "Thêm Khách Hàng";
            ((System.ComponentModel.ISupportInitialize)(this.GrbDangKy)).EndInit();
            this.GrbDangKy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DeNgaySinh.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbeMaGiamGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbeGioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeSDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeTenKH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeMaKH.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl LbDKTaiKhoan;
        private DevExpress.XtraEditors.GroupControl GrbDangKy;
        private DevExpress.XtraEditors.SimpleButton BtnXoaThongTin;
        private DevExpress.XtraEditors.SimpleButton BtnThem;
        private DevExpress.XtraEditors.DateEdit DeNgaySinh;
        private DevExpress.XtraEditors.LabelControl LbNgaySinh;
        private DevExpress.XtraEditors.ComboBoxEdit CbeMaGiamGia;
        private DevExpress.XtraEditors.ComboBoxEdit CbeGioiTinh;
        private DevExpress.XtraEditors.TextEdit TeDiaChi;
        private DevExpress.XtraEditors.LabelControl LbDiaChi;
        private DevExpress.XtraEditors.TextEdit TeSDT;
        private DevExpress.XtraEditors.LabelControl LbSDT;
        private DevExpress.XtraEditors.LabelControl LbGioiTinh;
        private DevExpress.XtraEditors.LabelControl LbMaGiamGia;
        private DevExpress.XtraEditors.TextEdit TeTenKH;
        private DevExpress.XtraEditors.LabelControl LbTenKH;
        private DevExpress.XtraEditors.TextEdit TeMaKH;
        private DevExpress.XtraEditors.LabelControl LbMaKH;
    }
}
﻿using DevExpress.XtraBars;
using market_management.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace market_management
{
    public partial class FrmMain : DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm
    {

        UcLoaiSanPham _UcLSP;
        UcSanPham _UcSP;
        UcTKBanHang _UcTKBanHang;
        UcQLNhapHang _UcQLNhapHang;
        UcKhachHang _UcKH;
        UcNhanVien _UcNV;
        UcDoanhThu _UcDT;
        UcQLMaGiamGia _UcMGG;

        public FrmMain()
        {
            InitializeComponent();
        }
        public int MaNV { get; set; }

        DataAccess dataAccess = new DataAccess();

        public void LoadNhanVienData()
        {
            string query = "SELECT TenNV, ChucVu FROM NHAN_VIEN WHERE NHAN_VIEN.MaNV = @MaNV";

            using (SqlCommand cmd = new SqlCommand(query, dataAccess.objConnection))
            {
                dataAccess.objConnection.Open();

                cmd.Parameters.AddWithValue("@MaNV", MaNV);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        BsiTenNV.Caption = reader["TenNV"].ToString();

                        Session.tenNV = reader["TenNV"].ToString();

                        BsiChucvu.Caption = reader["ChucVu"].ToString();

                        Session.chucVu = Convert.ToBoolean(reader["ChucVu"]);
                    }
                    else
                    {
                        MessageBox.Show("No data found.");
                    }
                }
                dataAccess.objConnection.Close();
            }
        }


        private void LoaiSP_Click(object sender, EventArgs e)
        {
            if (_UcLSP == null)
            {
                _UcLSP = new UcLoaiSanPham();
                _UcLSP.Dock = DockStyle.Fill;
                PnlMain.Controls.Add(_UcLSP);
                _UcLSP.BringToFront();
            }
            else
            {
                _UcLSP.BringToFront();
            }
        }

        private void SP_Click(object sender, EventArgs e)
        {
            if (_UcSP == null)
            {
                _UcSP = new UcSanPham();
                _UcSP.Dock = DockStyle.Fill;
                PnlMain.Controls.Add(_UcSP);
                _UcSP.BringToFront();
            }
            else
            {
                _UcSP.BringToFront();
            }
        }

        private void KH_Click(object sender, EventArgs e)
        {
            if (_UcKH == null)
            {
                _UcKH = new UcKhachHang();
                _UcKH.Dock = DockStyle.Fill;
                PnlMain.Controls.Add(_UcKH);
                _UcKH.BringToFront();
            }
            else
            {
                _UcKH.BringToFront();
            }
        }
        private void NV_Click(object sender, EventArgs e)
        {
            if (_UcNV == null)
            {
                _UcNV = new UcNhanVien();
                _UcNV.Dock = DockStyle.Fill;
                PnlMain.Controls.Add(_UcNV);
                _UcNV.BringToFront();
            }
            else
            {
                _UcNV.BringToFront();
            }
        }
        private void TKDoanhThu_Click(object sender, EventArgs e)
        {
            if (_UcDT == null)
            {
                _UcDT = new UcDoanhThu();
                _UcDT.Dock = DockStyle.Fill;
                PnlMain.Controls.Add(_UcDT);
                _UcDT.BringToFront();
            }
            else
            {
                _UcDT.BringToFront();
            }
        }

        private void TKHangTon_Click(object sender, EventArgs e)
        {

        }

        private void QLNhapHang_Click(object sender, EventArgs e)
        {
            if (_UcQLNhapHang == null)
            {
                _UcQLNhapHang = new UcQLNhapHang();
                _UcQLNhapHang.Dock = DockStyle.Fill;
                PnlMain.Controls.Add(_UcQLNhapHang);
                _UcQLNhapHang.BringToFront();
            }
            else
            {
                _UcQLNhapHang.BringToFront();
            }
        }

        private void QLMaGiamGia_Click(object sender, EventArgs e)
        {
            if (_UcMGG == null)
            {
                _UcMGG = new UcQLMaGiamGia();
                _UcMGG.Dock = DockStyle.Fill;
                PnlMain.Controls.Add(_UcMGG);
                _UcMGG.BringToFront();
            }
            else
            {
                _UcMGG.BringToFront();
            }
        }

        private void DangXuat_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmDangNhap frmDangNhap = new FrmDangNhap();
            frmDangNhap.ShowDialog();

        }

        private void DangKy_Click_1(object sender, EventArgs e)
        {
            FrmDangKy frmDangKy = new FrmDangKy();
            frmDangKy.ShowDialog();
        }

        private void TKBanHang_Click(object sender, EventArgs e)
        {
            if (_UcTKBanHang == null)
            {
                _UcTKBanHang = new UcTKBanHang();
                _UcTKBanHang.Dock = DockStyle.Fill;
                PnlMain.Controls.Add(_UcTKBanHang);
                _UcTKBanHang.BringToFront();
            }
            else
            {
                _UcTKBanHang.BringToFront();
            }
        }

        private void DangKy_Click(object sender, EventArgs e)
        {
            FrmDangKy f = new FrmDangKy();
            f.ShowDialog();
        }
        
    }
}

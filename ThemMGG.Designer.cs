﻿namespace market_management
{
    partial class FrmThemMGG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbDKTaiKhoan = new DevExpress.XtraEditors.LabelControl();
            this.GrbDangKy = new DevExpress.XtraEditors.GroupControl();
            this.BtnXoaDangKy = new DevExpress.XtraEditors.SimpleButton();
            this.BtnThem = new DevExpress.XtraEditors.SimpleButton();
            this.RbHetHan = new System.Windows.Forms.RadioButton();
            this.RbConHieuLuc = new System.Windows.Forms.RadioButton();
            this.TeMoTa = new DevExpress.XtraEditors.TextEdit();
            this.LbMoTa = new DevExpress.XtraEditors.LabelControl();
            this.LbTrangThai = new DevExpress.XtraEditors.LabelControl();
            this.DeNgayTao = new DevExpress.XtraEditors.DateEdit();
            this.CbePhanTram = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LbNgayTao = new DevExpress.XtraEditors.LabelControl();
            this.LbPhanTram = new DevExpress.XtraEditors.LabelControl();
            this.TeTenChuongTrinh = new DevExpress.XtraEditors.TextEdit();
            this.LbTenChuongTrinh = new DevExpress.XtraEditors.LabelControl();
            this.TeMaGiamGia = new DevExpress.XtraEditors.TextEdit();
            this.LbMaGiamGia = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.GrbDangKy)).BeginInit();
            this.GrbDangKy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeMoTa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgayTao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgayTao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbePhanTram.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeTenChuongTrinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeMaGiamGia.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LbDKTaiKhoan
            // 
            this.LbDKTaiKhoan.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LbDKTaiKhoan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.LbDKTaiKhoan.Appearance.Font = new System.Drawing.Font("Segoe UI", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbDKTaiKhoan.Appearance.ForeColor = System.Drawing.Color.Black;
            this.LbDKTaiKhoan.Appearance.Options.UseBackColor = true;
            this.LbDKTaiKhoan.Appearance.Options.UseFont = true;
            this.LbDKTaiKhoan.Appearance.Options.UseForeColor = true;
            this.LbDKTaiKhoan.Location = new System.Drawing.Point(377, 39);
            this.LbDKTaiKhoan.Name = "LbDKTaiKhoan";
            this.LbDKTaiKhoan.Size = new System.Drawing.Size(183, 31);
            this.LbDKTaiKhoan.TabIndex = 3;
            this.LbDKTaiKhoan.Text = "Đăng ký tài khoản";
            // 
            // GrbDangKy
            // 
            this.GrbDangKy.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GrbDangKy.Appearance.BackColor = System.Drawing.Color.Wheat;
            this.GrbDangKy.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.GrbDangKy.Appearance.Options.UseBackColor = true;
            this.GrbDangKy.Appearance.Options.UseFont = true;
            this.GrbDangKy.Controls.Add(this.RbHetHan);
            this.GrbDangKy.Controls.Add(this.RbConHieuLuc);
            this.GrbDangKy.Controls.Add(this.TeMoTa);
            this.GrbDangKy.Controls.Add(this.LbMoTa);
            this.GrbDangKy.Controls.Add(this.LbTrangThai);
            this.GrbDangKy.Controls.Add(this.DeNgayTao);
            this.GrbDangKy.Controls.Add(this.CbePhanTram);
            this.GrbDangKy.Controls.Add(this.LbNgayTao);
            this.GrbDangKy.Controls.Add(this.LbPhanTram);
            this.GrbDangKy.Controls.Add(this.TeTenChuongTrinh);
            this.GrbDangKy.Controls.Add(this.LbTenChuongTrinh);
            this.GrbDangKy.Controls.Add(this.TeMaGiamGia);
            this.GrbDangKy.Controls.Add(this.LbMaGiamGia);
            this.GrbDangKy.Controls.Add(this.BtnXoaDangKy);
            this.GrbDangKy.Controls.Add(this.BtnThem);
            this.GrbDangKy.Location = new System.Drawing.Point(35, 103);
            this.GrbDangKy.Name = "GrbDangKy";
            this.GrbDangKy.Size = new System.Drawing.Size(913, 313);
            this.GrbDangKy.TabIndex = 2;
            this.GrbDangKy.Text = "Thông tin đăng ký";
            // 
            // BtnXoaDangKy
            // 
            this.BtnXoaDangKy.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnXoaDangKy.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnXoaDangKy.Appearance.Options.UseFont = true;
            this.BtnXoaDangKy.Location = new System.Drawing.Point(86, 254);
            this.BtnXoaDangKy.Name = "BtnXoaDangKy";
            this.BtnXoaDangKy.Size = new System.Drawing.Size(137, 23);
            this.BtnXoaDangKy.TabIndex = 14;
            this.BtnXoaDangKy.Text = "Xoá hết thông tin";
            this.BtnXoaDangKy.Click += new System.EventHandler(this.BtnXoaDangKy_Click);
            // 
            // BtnThem
            // 
            this.BtnThem.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnThem.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnThem.Appearance.Options.UseFont = true;
            this.BtnThem.Location = new System.Drawing.Point(412, 254);
            this.BtnThem.Name = "BtnThem";
            this.BtnThem.Size = new System.Drawing.Size(137, 23);
            this.BtnThem.TabIndex = 13;
            this.BtnThem.Text = "Thêm";
            this.BtnThem.Click += new System.EventHandler(this.BtnThem_Click);
            // 
            // RbHetHan
            // 
            this.RbHetHan.BackColor = System.Drawing.Color.Transparent;
            this.RbHetHan.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RbHetHan.Location = new System.Drawing.Point(748, 65);
            this.RbHetHan.Name = "RbHetHan";
            this.RbHetHan.Size = new System.Drawing.Size(83, 20);
            this.RbHetHan.TabIndex = 75;
            this.RbHetHan.TabStop = true;
            this.RbHetHan.Text = "Hết hạn";
            this.RbHetHan.UseVisualStyleBackColor = false;
            // 
            // RbConHieuLuc
            // 
            this.RbConHieuLuc.BackColor = System.Drawing.Color.Transparent;
            this.RbConHieuLuc.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RbConHieuLuc.Location = new System.Drawing.Point(653, 64);
            this.RbConHieuLuc.Name = "RbConHieuLuc";
            this.RbConHieuLuc.Size = new System.Drawing.Size(101, 22);
            this.RbConHieuLuc.TabIndex = 74;
            this.RbConHieuLuc.TabStop = true;
            this.RbConHieuLuc.Text = "Còn hiệu lực";
            this.RbConHieuLuc.UseVisualStyleBackColor = false;
            // 
            // TeMoTa
            // 
            this.TeMoTa.Location = new System.Drawing.Point(653, 122);
            this.TeMoTa.Name = "TeMoTa";
            this.TeMoTa.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeMoTa.Properties.Appearance.Options.UseFont = true;
            this.TeMoTa.Size = new System.Drawing.Size(164, 22);
            this.TeMoTa.TabIndex = 73;
            // 
            // LbMoTa
            // 
            this.LbMoTa.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbMoTa.Appearance.Options.UseFont = true;
            this.LbMoTa.Location = new System.Drawing.Point(582, 123);
            this.LbMoTa.Name = "LbMoTa";
            this.LbMoTa.Size = new System.Drawing.Size(33, 15);
            this.LbMoTa.TabIndex = 72;
            this.LbMoTa.Text = "Mô Tả";
            // 
            // LbTrangThai
            // 
            this.LbTrangThai.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbTrangThai.Appearance.Options.UseFont = true;
            this.LbTrangThai.Location = new System.Drawing.Point(582, 67);
            this.LbTrangThai.Name = "LbTrangThai";
            this.LbTrangThai.Size = new System.Drawing.Size(54, 15);
            this.LbTrangThai.TabIndex = 71;
            this.LbTrangThai.Text = "Trạng Thái";
            // 
            // DeNgayTao
            // 
            this.DeNgayTao.EditValue = null;
            this.DeNgayTao.Location = new System.Drawing.Point(427, 123);
            this.DeNgayTao.Name = "DeNgayTao";
            this.DeNgayTao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeNgayTao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeNgayTao.Size = new System.Drawing.Size(122, 20);
            this.DeNgayTao.TabIndex = 70;
            // 
            // CbePhanTram
            // 
            this.CbePhanTram.Location = new System.Drawing.Point(427, 66);
            this.CbePhanTram.Name = "CbePhanTram";
            this.CbePhanTram.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbePhanTram.Properties.Appearance.Options.UseFont = true;
            this.CbePhanTram.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CbePhanTram.Size = new System.Drawing.Size(122, 22);
            this.CbePhanTram.TabIndex = 69;
            // 
            // LbNgayTao
            // 
            this.LbNgayTao.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbNgayTao.Appearance.Options.UseFont = true;
            this.LbNgayTao.Location = new System.Drawing.Point(342, 123);
            this.LbNgayTao.Name = "LbNgayTao";
            this.LbNgayTao.Size = new System.Drawing.Size(50, 15);
            this.LbNgayTao.TabIndex = 68;
            this.LbNgayTao.Text = "Ngày Tạo";
            // 
            // LbPhanTram
            // 
            this.LbPhanTram.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbPhanTram.Appearance.Options.UseFont = true;
            this.LbPhanTram.Location = new System.Drawing.Point(342, 67);
            this.LbPhanTram.Name = "LbPhanTram";
            this.LbPhanTram.Size = new System.Drawing.Size(56, 15);
            this.LbPhanTram.TabIndex = 67;
            this.LbPhanTram.Text = "Phần Trăm";
            // 
            // TeTenChuongTrinh
            // 
            this.TeTenChuongTrinh.Location = new System.Drawing.Point(193, 122);
            this.TeTenChuongTrinh.Name = "TeTenChuongTrinh";
            this.TeTenChuongTrinh.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeTenChuongTrinh.Properties.Appearance.Options.UseFont = true;
            this.TeTenChuongTrinh.Size = new System.Drawing.Size(128, 22);
            this.TeTenChuongTrinh.TabIndex = 66;
            // 
            // LbTenChuongTrinh
            // 
            this.LbTenChuongTrinh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbTenChuongTrinh.Appearance.Options.UseFont = true;
            this.LbTenChuongTrinh.Location = new System.Drawing.Point(86, 123);
            this.LbTenChuongTrinh.Name = "LbTenChuongTrinh";
            this.LbTenChuongTrinh.Size = new System.Drawing.Size(95, 15);
            this.LbTenChuongTrinh.TabIndex = 65;
            this.LbTenChuongTrinh.Text = "Tên Chương Trình";
            // 
            // TeMaGiamGia
            // 
            this.TeMaGiamGia.Location = new System.Drawing.Point(193, 66);
            this.TeMaGiamGia.Name = "TeMaGiamGia";
            this.TeMaGiamGia.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeMaGiamGia.Properties.Appearance.Options.UseFont = true;
            this.TeMaGiamGia.Size = new System.Drawing.Size(128, 22);
            this.TeMaGiamGia.TabIndex = 64;
            // 
            // LbMaGiamGia
            // 
            this.LbMaGiamGia.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbMaGiamGia.Appearance.Options.UseFont = true;
            this.LbMaGiamGia.Location = new System.Drawing.Point(86, 67);
            this.LbMaGiamGia.Name = "LbMaGiamGia";
            this.LbMaGiamGia.Size = new System.Drawing.Size(68, 15);
            this.LbMaGiamGia.TabIndex = 63;
            this.LbMaGiamGia.Text = "Mã Giảm Giá";
            // 
            // FrmThemMGG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 479);
            this.Controls.Add(this.LbDKTaiKhoan);
            this.Controls.Add(this.GrbDangKy);
            this.IconOptions.Image = global::market_management.Properties.Resources.bieuTuong;
            this.Name = "FrmThemMGG";
            this.Text = "Thêm Mã Giảm Giá";
            ((System.ComponentModel.ISupportInitialize)(this.GrbDangKy)).EndInit();
            this.GrbDangKy.ResumeLayout(false);
            this.GrbDangKy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeMoTa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgayTao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgayTao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbePhanTram.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeTenChuongTrinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeMaGiamGia.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl LbDKTaiKhoan;
        private DevExpress.XtraEditors.GroupControl GrbDangKy;
        private DevExpress.XtraEditors.SimpleButton BtnXoaDangKy;
        private DevExpress.XtraEditors.SimpleButton BtnThem;
        private System.Windows.Forms.RadioButton RbHetHan;
        private System.Windows.Forms.RadioButton RbConHieuLuc;
        private DevExpress.XtraEditors.TextEdit TeMoTa;
        private DevExpress.XtraEditors.LabelControl LbMoTa;
        private DevExpress.XtraEditors.LabelControl LbTrangThai;
        private DevExpress.XtraEditors.DateEdit DeNgayTao;
        private DevExpress.XtraEditors.ComboBoxEdit CbePhanTram;
        private DevExpress.XtraEditors.LabelControl LbNgayTao;
        private DevExpress.XtraEditors.LabelControl LbPhanTram;
        private DevExpress.XtraEditors.TextEdit TeTenChuongTrinh;
        private DevExpress.XtraEditors.LabelControl LbTenChuongTrinh;
        private DevExpress.XtraEditors.TextEdit TeMaGiamGia;
        private DevExpress.XtraEditors.LabelControl LbMaGiamGia;
    }
}
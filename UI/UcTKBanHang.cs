﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace market_management.UI
{
    public partial class UcTKBanHang : DevExpress.XtraEditors.XtraUserControl
    {
        public UcTKBanHang()
        {
            InitializeComponent();
            LoadData();
        }
        DataAccess dataAccess = new DataAccess();

        private void LoadData()
        {
            // Bảng sản phẩm có số lượng bán từ cao đến thấp
            DataTable dataTable = GetSanPhamDaBanDataTable();
            GcSanPhamDaBan.DataSource = dataTable;
        }

        private DataTable GetSanPhamDaBanDataTable()
        {
            DataTable dataTable = new DataTable();

            string query = "SELECT SP.MaSP, SP.TenSP, SP.SoLuong, SUM(CT.SoLuong) AS SoLuongDaBan " +
                           "FROM SAN_PHAM SP " +
                           "JOIN CT_HOA_DON_BAN CT ON SP.MaSP = CT.MaSP " +
                           "GROUP BY SP.MaSP, SP.TenSP, SP.SoLuong " +
                           "ORDER BY SoLuongDaBan DESC";

            // Sử dụng DataAccess thay vì SqlConnection và SqlDataAdapter
            dataTable = dataAccess.GetDataTable(query);

            return dataTable;
        }
        private void UcTKBanHang_Load(object sender, EventArgs e)
        {
        }
    }
}

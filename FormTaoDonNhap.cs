﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace market_management
{
    public partial class FrmTaoDonNhap : DevExpress.XtraEditors.XtraForm
    {
        DataAccess dataAccess = new DataAccess();
        System.Data.DataTable dataTable;
        public FrmTaoDonNhap()
        {
            InitializeComponent();
        }

        private void BtnTaoHD_Click(object sender, EventArgs e)
        {
            dataAccess.UpdateData($"INSERT INTO HOA_DON_NHAP (MaHDN, MaNCC, MaNV, TongTien, " +
                $"ThoiGian) VALUES ()");

            string maHDN = GenerateRandomString(8);
            string maNCC = ""; 
            string tenNCC = CmbTenNCC.Text;

            string query = $"SELECT MaNCC FROM NHA_CUNG_CAP WHERE TenNCC = '{tenNCC}'";

            using (SqlCommand cmd = new SqlCommand(query, dataAccess.objConnection))
            {
                dataAccess.objConnection.Open();

                object result = cmd.ExecuteScalar();
                if (result != null)
                {
                    maNCC = result.ToString();
                }

                dataAccess.objConnection.Close();
            }
            string maNV = Session.luuMaNV.ToString();

            double tongTien = TinhTongTien();

            DateTime thoiGian = DateTime.Now;

            dataAccess.UpdateData($"INSERT INTO HOA_DON_NHAP (MaHDN, MaNCC, MaNV, TongTien, ThoiGian) " +
                                  $"VALUES ('{maHDN}', '{maNCC}', '{maNV}', {tongTien}, '{thoiGian}')");
            
            foreach (DataRow row in dataTable.Rows)
            {
                int maSP = Convert.ToInt32(row["Mã Sản Phẩm"]);
                int soLuong = Convert.ToInt32(row["Số Lượng"]);
                decimal giaNhap = Convert.ToDecimal(row["Giá Nhập"]);

                long thanhTien = Convert.ToInt64(giaNhap * soLuong);

                dataAccess.UpdateData($"INSERT INTO CT_HOA_DON_NHAP (MaHDN, MaSP, SoLuong, ThanhTien) " +
                                      $"VALUES ('{maHDN}', {maSP}, {soLuong}, {thanhTien})");
            }
            CapNhatSanPham();
            MessageBox.Show($"Hóa đơn đã được tạo thành công!\nMã hóa đơn: {maHDN}", "Thông báo", 
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }








        
        private List<string> LayTenNCC()
        {
            List<string> TenNCC = new List<string>();
            string query = "SELECT TenNCC FROM NHA_CUNG_CAP";

            using (SqlCommand cmd = new SqlCommand(query, dataAccess.objConnection))
            {
                dataAccess.objConnection.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TenNCC.Add(reader["TenNCC"].ToString());
                    }
                }
                dataAccess.objConnection.Close();
            }
            return TenNCC;
        }
        private void HienThiTenNCC()
        {
            List<string> TenNCC = LayTenNCC();
            foreach (string item in TenNCC)
            {
                CmbTenNCC.Items.Add(item);
            }
        }
        /*private List<string> LayTenSP()
        {
            List<string> TenSP = new List<string>();
            string query = "SELECT TenSP FROM SAN_PHAM";

            using (SqlCommand cmd = new SqlCommand(query, dataAccess.objConnection))
            {
                dataAccess.objConnection.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TenSP.Add(reader["TenSP"].ToString());
                    }
                }

                dataAccess.objConnection.Close();

            }
            return TenSP;
        }
        */
        /*private void HienThiTenSP()
        {
            List<string> TenLoaiSP = LayTenSP();
            CbeTenSP.Properties.Items.AddRange(TenLoaiSP);

            CbeTenSP.Properties.AutoComplete = true;
            CbeTenSP.Properties.CaseSensitiveSearch = false;
        }
        */
        private void CmbTenNCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            string tenNCC = CmbTenNCC.Text;

            string query = $"SELECT MaLoaiSP FROM NHA_CUNG_CAP WHERE TenNCC = '{tenNCC}'";

            string maLoaiSP = "";
            using (SqlCommand cmd = new SqlCommand(query, dataAccess.objConnection))
            {
                dataAccess.objConnection.Open();

                // Thực hiện truy vấn và đọc giá trị MaLoaiSP
                object result = cmd.ExecuteScalar();
                if (result != null)
                {
                    maLoaiSP = result.ToString();
                }

                dataAccess.objConnection.Close();
            }

            LoadProductsByMaLoaiSP(maLoaiSP);
        }
        private void LoadProductsByMaLoaiSP(string maLoaiSP)
        {
            string query = $"SELECT TenSP FROM SAN_PHAM WHERE MaLoaiSP = '{maLoaiSP}'";

            List<string> tenSPList = new List<string>();
            using (SqlCommand cmd = new SqlCommand(query, dataAccess.objConnection))
            {
                dataAccess.objConnection.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tenSPList.Add(reader["TenSP"].ToString());
                    }
                }

                dataAccess.objConnection.Close();
            }

            CbeTenSP.Properties.Items.Clear();
            CbeTenSP.Properties.Items.AddRange(tenSPList);
            CbeTenSP.Properties.AutoComplete = true;
            CbeTenSP.Properties.CaseSensitiveSearch = false;
        }






        private void FrmTaoDonNhap_Load(object sender, EventArgs e)
        {
            HienThiTenNCC();
            //HienThiTenSP();

            LbcTenNV.Text = Session.tenNV;

            timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += Timer_Tick;
            timer.Start();

            while (CmbTenNCC.Text != null)
            {
                CmbTenNCC.SelectedIndexChanged += CmbTenNCC_SelectedIndexChanged;

                //string maHDN = GenerateRandomString(8);

                dataTable = new System.Data.DataTable();
                dataTable.Columns.Add("Mã Sản Phẩm", typeof(int));
                dataTable.Columns.Add("Tên Sản Phẩm", typeof(string));
                dataTable.Columns.Add("Giá Nhập", typeof(decimal));
                dataTable.Columns.Add("Số Lượng", typeof(int));

                TeSoLuong.KeyPress += TeSoLuong_KeyPress;

                GcHDN.DataSource = dataTable;
            }
            
            
        }
        private void BtnThemSP_Click(object sender, EventArgs e)
        {
            string tenSP = CbeTenSP.Text;

            if (string.IsNullOrEmpty(tenSP))
            {
                MessageBox.Show("Bạn phải chọn một sản phẩm trước khi thêm vào hóa đơn.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return; 
            }

            string query = $"SELECT MaSP AS 'Mã Sản Phẩm', TenSP AS 'Tên Sản Phẩm', GiaNhap AS 'Giá Nhập', SoLuong AS 'Số Lượng' FROM SAN_PHAM WHERE TenSP = '{tenSP}'";

            System.Data.DataTable sanpham = dataAccess.GetDataTable(query);

            if (sanpham.Rows.Count > 0)
            {
                // Thêm dòng vào DataTable
                DataRow newRow = dataTable.NewRow();
                newRow["Mã Sản Phẩm"] = sanpham.Rows[0]["Mã Sản Phẩm"];
                newRow["Tên Sản Phẩm"] = sanpham.Rows[0]["Tên Sản Phẩm"];
                newRow["Số Lượng"] = TeSoLuong.Text;
                newRow["Giá Nhập"] = sanpham.Rows[0]["Giá Nhập"];

                dataTable.Rows.Add(newRow);
            }

            //LbcTongTien.Text = TinhTongTien().ToString();
            LbcTongTien.Text = TinhTongTien().ToString("N0");
            LbcTongTien.Text = string.Format("{0:#,##0}", double.Parse(LbcTongTien.Text));
        }
        private double TinhTongTien()
        {
            double totalIncome = 0;

            // Duyệt qua tất cả các dòng trong DataTable và tính tổng thu nhập
            foreach (DataRow row in dataTable.Rows)
            {
                if (row["Giá Nhập"] != DBNull.Value)
                {
                    totalIncome += Convert.ToDouble(row["Giá Nhập"]) * Convert.ToDouble(row["Số Lượng"]);
                }
            }

            return totalIncome;
        }
        private void CapNhatSanPham()
        {
            foreach (DataRow row in dataTable.Rows)
            {
                int maSP = Convert.ToInt32(row["Mã Sản Phẩm"]);
                int soluongNhap = Convert.ToInt32(row["Số Lượng"]);
                dataAccess.UpdateData(string.Format($"UPDATE SAN_PHAM SET SoLuong = SoLuong - {soluongNhap} where MaSP = {maSP}"));
            }
        }
        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            LbcTongTien.Text = TinhTongTien().ToString();
        }
        private void gridView1_RowCountChanged(object sender, EventArgs e)
        {
            LbcTongTien.Text = TinhTongTien().ToString();
        }
        static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            Random random = new Random();
            char[] randomArray = new char[length];

            for (int i = 0; i < length; i++)
            {
                randomArray[i] = chars[random.Next(chars.Length)];
            }
            return new string(randomArray);
        }
        private Timer timer;
        private void Timer_Tick(object sender, EventArgs e)
        {
            LbcThoiGian.Text = DateTime.Now.ToString("HH:mm:ss");
        }
        private void groupControl2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TeSoLuong_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}

﻿namespace market_management
{
    partial class FrmThemLoaiSP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CmbTrangThai = new System.Windows.Forms.ComboBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LbTenLSP = new DevExpress.XtraEditors.LabelControl();
            this.BtnThem = new System.Windows.Forms.Button();
            this.TeTenLoaiSP = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.TeTenLoaiSP.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // CmbTrangThai
            // 
            this.CmbTrangThai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbTrangThai.FormattingEnabled = true;
            this.CmbTrangThai.Items.AddRange(new object[] {
            "Đang kinh doanh",
            "Không còn kinh doanh"});
            this.CmbTrangThai.Location = new System.Drawing.Point(254, 155);
            this.CmbTrangThai.Name = "CmbTrangThai";
            this.CmbTrangThai.Size = new System.Drawing.Size(199, 26);
            this.CmbTrangThai.TabIndex = 52;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(120, 158);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(67, 18);
            this.labelControl2.TabIndex = 51;
            this.labelControl2.Text = "Trạng thái";
            // 
            // LbTenLSP
            // 
            this.LbTenLSP.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbTenLSP.Appearance.Options.UseFont = true;
            this.LbTenLSP.Location = new System.Drawing.Point(119, 103);
            this.LbTenLSP.Name = "LbTenLSP";
            this.LbTenLSP.Size = new System.Drawing.Size(120, 20);
            this.LbTenLSP.TabIndex = 47;
            this.LbTenLSP.Text = "Tên loại sản phẩm";
            // 
            // BtnThem
            // 
            this.BtnThem.Location = new System.Drawing.Point(191, 226);
            this.BtnThem.Name = "BtnThem";
            this.BtnThem.Size = new System.Drawing.Size(151, 30);
            this.BtnThem.TabIndex = 53;
            this.BtnThem.Text = "Thêm loại sản phẩm";
            this.BtnThem.UseVisualStyleBackColor = true;
            this.BtnThem.Click += new System.EventHandler(this.BtnThem_Click);
            // 
            // TeTenLoaiSP
            // 
            this.TeTenLoaiSP.Location = new System.Drawing.Point(254, 103);
            this.TeTenLoaiSP.Name = "TeTenLoaiSP";
            this.TeTenLoaiSP.Size = new System.Drawing.Size(199, 22);
            this.TeTenLoaiSP.TabIndex = 54;
            // 
            // FrmThemLoaiSP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 338);
            this.Controls.Add(this.TeTenLoaiSP);
            this.Controls.Add(this.BtnThem);
            this.Controls.Add(this.CmbTrangThai);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.LbTenLSP);
            this.Name = "FrmThemLoaiSP";
            this.Text = "FormThemLoaiSP";
            ((System.ComponentModel.ISupportInitialize)(this.TeTenLoaiSP.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CmbTrangThai;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl LbTenLSP;
        private System.Windows.Forms.Button BtnThem;
        private DevExpress.XtraEditors.TextEdit TeTenLoaiSP;
    }
}
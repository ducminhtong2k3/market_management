﻿namespace market_management
{
    partial class FrmThemNV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbThemNV = new DevExpress.XtraEditors.LabelControl();
            this.GrbDangKy = new DevExpress.XtraEditors.GroupControl();
            this.BtnThem = new DevExpress.XtraEditors.SimpleButton();
            this.DeNgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.CbeChucVu = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CbeGioiTinh = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LbChucVu = new DevExpress.XtraEditors.LabelControl();
            this.TeCCCD = new DevExpress.XtraEditors.TextEdit();
            this.LbCCCD = new DevExpress.XtraEditors.LabelControl();
            this.TeDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.LbDiaChi = new DevExpress.XtraEditors.LabelControl();
            this.TeEmail = new DevExpress.XtraEditors.TextEdit();
            this.LbEmail = new DevExpress.XtraEditors.LabelControl();
            this.TeSDT = new DevExpress.XtraEditors.TextEdit();
            this.LbSDT = new DevExpress.XtraEditors.LabelControl();
            this.LbGioiTInh = new DevExpress.XtraEditors.LabelControl();
            this.LbNgaySinh = new DevExpress.XtraEditors.LabelControl();
            this.TeTenNV = new DevExpress.XtraEditors.TextEdit();
            this.LbTenNV = new DevExpress.XtraEditors.LabelControl();
            this.TeMaNV = new DevExpress.XtraEditors.TextEdit();
            this.LbMaNV = new DevExpress.XtraEditors.LabelControl();
            this.BtnXoaThongTin = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.GrbDangKy)).BeginInit();
            this.GrbDangKy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgaySinh.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbeChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbeGioiTinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeCCCD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeSDT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeTenNV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeMaNV.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LbThemNV
            // 
            this.LbThemNV.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LbThemNV.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.LbThemNV.Appearance.Font = new System.Drawing.Font("Segoe UI", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbThemNV.Appearance.ForeColor = System.Drawing.Color.Black;
            this.LbThemNV.Appearance.Options.UseBackColor = true;
            this.LbThemNV.Appearance.Options.UseFont = true;
            this.LbThemNV.Appearance.Options.UseForeColor = true;
            this.LbThemNV.Location = new System.Drawing.Point(387, 50);
            this.LbThemNV.Name = "LbThemNV";
            this.LbThemNV.Size = new System.Drawing.Size(162, 31);
            this.LbThemNV.TabIndex = 5;
            this.LbThemNV.Text = "Thêm nhân viên";
            // 
            // GrbDangKy
            // 
            this.GrbDangKy.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GrbDangKy.Appearance.BackColor = System.Drawing.Color.Wheat;
            this.GrbDangKy.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.GrbDangKy.Appearance.Options.UseBackColor = true;
            this.GrbDangKy.Appearance.Options.UseFont = true;
            this.GrbDangKy.Controls.Add(this.BtnXoaThongTin);
            this.GrbDangKy.Controls.Add(this.BtnThem);
            this.GrbDangKy.Location = new System.Drawing.Point(56, 109);
            this.GrbDangKy.Name = "GrbDangKy";
            this.GrbDangKy.Size = new System.Drawing.Size(832, 313);
            this.GrbDangKy.TabIndex = 4;
            this.GrbDangKy.Text = "Thông tin nhân viên";
            // 
            // BtnThem
            // 
            this.BtnThem.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnThem.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnThem.Appearance.Options.UseFont = true;
            this.BtnThem.Location = new System.Drawing.Point(365, 270);
            this.BtnThem.Name = "BtnThem";
            this.BtnThem.Size = new System.Drawing.Size(122, 23);
            this.BtnThem.TabIndex = 13;
            this.BtnThem.Text = "Thêm";
            // 
            // DeNgaySinh
            // 
            this.DeNgaySinh.EditValue = null;
            this.DeNgaySinh.Location = new System.Drawing.Point(183, 277);
            this.DeNgaySinh.Name = "DeNgaySinh";
            this.DeNgaySinh.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeNgaySinh.Properties.Appearance.Options.UseFont = true;
            this.DeNgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeNgaySinh.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DeNgaySinh.Size = new System.Drawing.Size(128, 22);
            this.DeNgaySinh.TabIndex = 56;
            // 
            // CbeChucVu
            // 
            this.CbeChucVu.Location = new System.Drawing.Point(688, 277);
            this.CbeChucVu.Name = "CbeChucVu";
            this.CbeChucVu.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbeChucVu.Properties.Appearance.Options.UseFont = true;
            this.CbeChucVu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CbeChucVu.Size = new System.Drawing.Size(144, 22);
            this.CbeChucVu.TabIndex = 55;
            // 
            // CbeGioiTinh
            // 
            this.CbeGioiTinh.Location = new System.Drawing.Point(421, 169);
            this.CbeGioiTinh.Name = "CbeGioiTinh";
            this.CbeGioiTinh.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbeGioiTinh.Properties.Appearance.Options.UseFont = true;
            this.CbeGioiTinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CbeGioiTinh.Size = new System.Drawing.Size(122, 22);
            this.CbeGioiTinh.TabIndex = 54;
            // 
            // LbChucVu
            // 
            this.LbChucVu.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbChucVu.Appearance.Options.UseFont = true;
            this.LbChucVu.Location = new System.Drawing.Point(569, 278);
            this.LbChucVu.Name = "LbChucVu";
            this.LbChucVu.Size = new System.Drawing.Size(45, 15);
            this.LbChucVu.TabIndex = 53;
            this.LbChucVu.Text = "Chức Vụ";
            // 
            // TeCCCD
            // 
            this.TeCCCD.Location = new System.Drawing.Point(688, 223);
            this.TeCCCD.Name = "TeCCCD";
            this.TeCCCD.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeCCCD.Properties.Appearance.Options.UseFont = true;
            this.TeCCCD.Size = new System.Drawing.Size(144, 22);
            this.TeCCCD.TabIndex = 52;
            // 
            // LbCCCD
            // 
            this.LbCCCD.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCCCD.Appearance.Options.UseFont = true;
            this.LbCCCD.Location = new System.Drawing.Point(569, 226);
            this.LbCCCD.Name = "LbCCCD";
            this.LbCCCD.Size = new System.Drawing.Size(103, 15);
            this.LbCCCD.TabIndex = 51;
            this.LbCCCD.Text = "Căn cước công dân";
            // 
            // TeDiaChi
            // 
            this.TeDiaChi.Location = new System.Drawing.Point(688, 167);
            this.TeDiaChi.Name = "TeDiaChi";
            this.TeDiaChi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeDiaChi.Properties.Appearance.Options.UseFont = true;
            this.TeDiaChi.Size = new System.Drawing.Size(144, 22);
            this.TeDiaChi.TabIndex = 50;
            // 
            // LbDiaChi
            // 
            this.LbDiaChi.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbDiaChi.Appearance.Options.UseFont = true;
            this.LbDiaChi.Location = new System.Drawing.Point(569, 170);
            this.LbDiaChi.Name = "LbDiaChi";
            this.LbDiaChi.Size = new System.Drawing.Size(36, 15);
            this.LbDiaChi.TabIndex = 49;
            this.LbDiaChi.Text = "Địa chỉ";
            // 
            // TeEmail
            // 
            this.TeEmail.Location = new System.Drawing.Point(421, 275);
            this.TeEmail.Name = "TeEmail";
            this.TeEmail.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeEmail.Properties.Appearance.Options.UseFont = true;
            this.TeEmail.Size = new System.Drawing.Size(122, 22);
            this.TeEmail.TabIndex = 48;
            // 
            // LbEmail
            // 
            this.LbEmail.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbEmail.Appearance.Options.UseFont = true;
            this.LbEmail.Location = new System.Drawing.Point(336, 278);
            this.LbEmail.Name = "LbEmail";
            this.LbEmail.Size = new System.Drawing.Size(29, 15);
            this.LbEmail.TabIndex = 47;
            this.LbEmail.Text = "Email";
            // 
            // TeSDT
            // 
            this.TeSDT.Location = new System.Drawing.Point(421, 223);
            this.TeSDT.Name = "TeSDT";
            this.TeSDT.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeSDT.Properties.Appearance.Options.UseFont = true;
            this.TeSDT.Size = new System.Drawing.Size(122, 22);
            this.TeSDT.TabIndex = 46;
            // 
            // LbSDT
            // 
            this.LbSDT.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbSDT.Appearance.Options.UseFont = true;
            this.LbSDT.Location = new System.Drawing.Point(336, 226);
            this.LbSDT.Name = "LbSDT";
            this.LbSDT.Size = new System.Drawing.Size(69, 15);
            this.LbSDT.TabIndex = 45;
            this.LbSDT.Text = "Số điện thoại";
            // 
            // LbGioiTInh
            // 
            this.LbGioiTInh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbGioiTInh.Appearance.Options.UseFont = true;
            this.LbGioiTInh.Location = new System.Drawing.Point(336, 170);
            this.LbGioiTInh.Name = "LbGioiTInh";
            this.LbGioiTInh.Size = new System.Drawing.Size(45, 15);
            this.LbGioiTInh.TabIndex = 44;
            this.LbGioiTInh.Text = "Giới tính";
            // 
            // LbNgaySinh
            // 
            this.LbNgaySinh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbNgaySinh.Appearance.Options.UseFont = true;
            this.LbNgaySinh.Location = new System.Drawing.Point(87, 278);
            this.LbNgaySinh.Name = "LbNgaySinh";
            this.LbNgaySinh.Size = new System.Drawing.Size(54, 15);
            this.LbNgaySinh.TabIndex = 43;
            this.LbNgaySinh.Text = "Ngày Sinh";
            // 
            // TeTenNV
            // 
            this.TeTenNV.Location = new System.Drawing.Point(183, 225);
            this.TeTenNV.Name = "TeTenNV";
            this.TeTenNV.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.TeTenNV.Properties.Appearance.Options.UseFont = true;
            this.TeTenNV.Size = new System.Drawing.Size(128, 22);
            this.TeTenNV.TabIndex = 42;
            // 
            // LbTenNV
            // 
            this.LbTenNV.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbTenNV.Appearance.Options.UseFont = true;
            this.LbTenNV.Location = new System.Drawing.Point(87, 226);
            this.LbTenNV.Name = "LbTenNV";
            this.LbTenNV.Size = new System.Drawing.Size(77, 15);
            this.LbTenNV.TabIndex = 41;
            this.LbTenNV.Text = "Tên Nhân Viên";
            // 
            // TeMaNV
            // 
            this.TeMaNV.Location = new System.Drawing.Point(183, 169);
            this.TeMaNV.Name = "TeMaNV";
            this.TeMaNV.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeMaNV.Properties.Appearance.Options.UseFont = true;
            this.TeMaNV.Size = new System.Drawing.Size(128, 22);
            this.TeMaNV.TabIndex = 40;
            // 
            // LbMaNV
            // 
            this.LbMaNV.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbMaNV.Appearance.Options.UseFont = true;
            this.LbMaNV.Location = new System.Drawing.Point(87, 170);
            this.LbMaNV.Name = "LbMaNV";
            this.LbMaNV.Size = new System.Drawing.Size(75, 15);
            this.LbMaNV.TabIndex = 39;
            this.LbMaNV.Text = "Mã Nhân Viên";
            // 
            // BtnXoaThongTin
            // 
            this.BtnXoaThongTin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnXoaThongTin.Appearance.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnXoaThongTin.Appearance.Options.UseFont = true;
            this.BtnXoaThongTin.Location = new System.Drawing.Point(87, 270);
            this.BtnXoaThongTin.Name = "BtnXoaThongTin";
            this.BtnXoaThongTin.Size = new System.Drawing.Size(137, 23);
            this.BtnXoaThongTin.TabIndex = 14;
            this.BtnXoaThongTin.Text = "Xoá hết thông tin";
            this.BtnXoaThongTin.Click += new System.EventHandler(this.BtnXoaThongTin_Click);
            // 
            // FrmThemNV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 467);
            this.Controls.Add(this.DeNgaySinh);
            this.Controls.Add(this.CbeChucVu);
            this.Controls.Add(this.CbeGioiTinh);
            this.Controls.Add(this.LbChucVu);
            this.Controls.Add(this.TeCCCD);
            this.Controls.Add(this.LbCCCD);
            this.Controls.Add(this.TeDiaChi);
            this.Controls.Add(this.LbDiaChi);
            this.Controls.Add(this.TeEmail);
            this.Controls.Add(this.LbEmail);
            this.Controls.Add(this.TeSDT);
            this.Controls.Add(this.LbSDT);
            this.Controls.Add(this.LbGioiTInh);
            this.Controls.Add(this.LbNgaySinh);
            this.Controls.Add(this.TeTenNV);
            this.Controls.Add(this.LbTenNV);
            this.Controls.Add(this.TeMaNV);
            this.Controls.Add(this.LbMaNV);
            this.Controls.Add(this.LbThemNV);
            this.Controls.Add(this.GrbDangKy);
            this.IconOptions.Image = global::market_management.Properties.Resources.bieuTuong;
            this.Name = "FrmThemNV";
            this.Text = "Thêm Nhân Viên";
            ((System.ComponentModel.ISupportInitialize)(this.GrbDangKy)).EndInit();
            this.GrbDangKy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DeNgaySinh.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbeChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbeGioiTinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeCCCD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeSDT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeTenNV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeMaNV.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl LbThemNV;
        private DevExpress.XtraEditors.GroupControl GrbDangKy;
        private DevExpress.XtraEditors.SimpleButton BtnThem;
        private DevExpress.XtraEditors.DateEdit DeNgaySinh;
        private DevExpress.XtraEditors.ComboBoxEdit CbeChucVu;
        private DevExpress.XtraEditors.ComboBoxEdit CbeGioiTinh;
        private DevExpress.XtraEditors.LabelControl LbChucVu;
        private DevExpress.XtraEditors.TextEdit TeCCCD;
        private DevExpress.XtraEditors.LabelControl LbCCCD;
        private DevExpress.XtraEditors.TextEdit TeDiaChi;
        private DevExpress.XtraEditors.LabelControl LbDiaChi;
        private DevExpress.XtraEditors.TextEdit TeEmail;
        private DevExpress.XtraEditors.LabelControl LbEmail;
        private DevExpress.XtraEditors.TextEdit TeSDT;
        private DevExpress.XtraEditors.LabelControl LbSDT;
        private DevExpress.XtraEditors.LabelControl LbGioiTInh;
        private DevExpress.XtraEditors.LabelControl LbNgaySinh;
        private DevExpress.XtraEditors.TextEdit TeTenNV;
        private DevExpress.XtraEditors.LabelControl LbTenNV;
        private DevExpress.XtraEditors.TextEdit TeMaNV;
        private DevExpress.XtraEditors.LabelControl LbMaNV;
        private DevExpress.XtraEditors.SimpleButton BtnXoaThongTin;
    }
}